package nl.underkoen.dreammagic;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import nl.underkoen.dreammagic.blocks.BlockRegistration;
import nl.underkoen.dreammagic.items.ItemRegistration;
import nl.underkoen.dreammagic.util.Util;
import org.apache.logging.log4j.Logger;

@Mod(modid = DreamMagic.MOD_ID, name = DreamMagic.NAME, version = DreamMagic.VERSION)
public class DreamMagic {
    public static final String MOD_ID = "dreammagic";
    public static final String NAME = "Dream Magic";
    public static final String VERSION = "1.0";

    public static Logger logger;

    public static CreativeTabs creativeTab = new CreativeTabs(MOD_ID) {
        @Override
        @SideOnly(Side.CLIENT)
        public ItemStack getTabIconItem() {
            return new ItemStack(BlockRegistration.cloud);
        }
    };

    @SidedProxy(serverSide = "nl.underkoen.dreammagic.util.ServerUtil", clientSide = "nl.underkoen.dreammagic.util.ClientUtil")
    public static Util proxyUtil;


    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        MinecraftForge.EVENT_BUS.register(this);
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        logger.info("Initialing DreamMagic");
    }

    @SubscribeEvent
    public void registerItems(RegistryEvent.Register<Item> event) {
        ItemRegistration.init(event);
    }

    @SubscribeEvent
    public void registerBlock(RegistryEvent.Register<Block> event) {
        BlockRegistration.init(event);
    }
}
