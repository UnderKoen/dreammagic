package nl.underkoen.dreammagic.util;

public class ServerUtil extends Util {
    @Override
    public boolean isServer() {
        return true;
    }

    @Override
    public boolean isClient() {
        return false;
    }
}
