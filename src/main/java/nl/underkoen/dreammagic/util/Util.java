package nl.underkoen.dreammagic.util;

public abstract class Util {
    Util() {
    }

    public abstract boolean isServer();

    public abstract boolean isClient();
}
