package nl.underkoen.dreammagic.util;

public class ClientUtil extends Util {
    @Override
    public boolean isServer() {
        return false;
    }

    @Override
    public boolean isClient() {
        return true;
    }
}
