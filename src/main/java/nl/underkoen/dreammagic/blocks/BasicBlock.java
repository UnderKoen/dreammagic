package nl.underkoen.dreammagic.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import nl.underkoen.dreammagic.DreamMagic;

public class BasicBlock extends Block {
    public BasicBlock(String name, Material material) {
        super(material);
        setUnlocalizedName(DreamMagic.MOD_ID + "." + name);
        setRegistryName(new ResourceLocation(DreamMagic.MOD_ID, name));
    }
}
