package nl.underkoen.dreammagic.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.event.RegistryEvent;
import nl.underkoen.dreammagic.DreamMagic;

import java.util.Arrays;

public class BlockRegistration {
    public static Block cloud;

    public static void init(RegistryEvent.Register<Block> event) {
        cloud = new BasicBlock("cloud", Material.ROCK);

        Arrays.stream(getBlocks()).forEach(block -> block.setCreativeTab(DreamMagic.creativeTab));
        Arrays.stream(getBlocks()).forEach(block -> event.getRegistry().register(block));
    }

    public static Block[] getBlocks() {
        return new Block[]{
                cloud
        };
    }
}
