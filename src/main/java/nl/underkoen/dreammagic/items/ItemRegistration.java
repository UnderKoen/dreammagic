package nl.underkoen.dreammagic.items;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import nl.underkoen.dreammagic.DreamMagic;
import nl.underkoen.dreammagic.blocks.BlockRegistration;

import java.util.*;

public class ItemRegistration {
    public static List<ItemBlock> itemBlocks;

    public static void init(RegistryEvent.Register<Item> event) {
        initBlocks();

        getItems().forEach(item -> {
            item.setCreativeTab(DreamMagic.creativeTab);
            event.getRegistry().register(item);
        });
    }

    public static void initBlocks() {
        itemBlocks = new ArrayList<>();
        Arrays.stream(BlockRegistration.getBlocks()).forEach(block -> {
            Item blockItem = new ItemBlock(block).setRegistryName(Objects.requireNonNull(block.getRegistryName()));
            itemBlocks.add((ItemBlock) blockItem);
        });
    }

    public static Item[] getOnlyItems() {
        return new Item[]{

        };
    }

    public static Collection<Item> getItems() {
        Collection<Item> items = new ArrayList<>(itemBlocks);

        items.addAll(Arrays.asList(getOnlyItems()));

        return items;
    }
}
