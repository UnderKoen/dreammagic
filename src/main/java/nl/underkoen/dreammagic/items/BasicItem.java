package nl.underkoen.dreammagic.items;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import nl.underkoen.dreammagic.DreamMagic;

public class BasicItem extends Item {
    public BasicItem(String name) {
        setUnlocalizedName(DreamMagic.MOD_ID + "." + name);
        setRegistryName(new ResourceLocation(DreamMagic.MOD_ID, name));
    }
}
